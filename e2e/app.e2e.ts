import { GladpetPage } from './app.po';

describe('gladpet App', function() {
  let page: GladpetPage;

  beforeEach(() => {
    page = new GladpetPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('gladpet works!');
  });
});
