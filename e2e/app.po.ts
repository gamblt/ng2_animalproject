export class GladpetPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('gladpet-app h1')).getText();
  }
}
