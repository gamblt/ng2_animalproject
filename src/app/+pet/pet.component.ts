import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-pet',
  templateUrl: 'pet.component.html',
  styleUrls: ['pet.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
export class PetComponent implements OnInit {

  constructor() {
    console.log('Init Pet component!');
  }

  ngOnInit() {
  }

}
