import { Component } from '@angular/core';
import { Routes, Route, Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';
import { PetComponent } from './+pet';
import { AnimalComponent } from './+animal';
import { IndexComponent } from './+index';

@Component({
  moduleId: module.id,
  selector: 'gladpet-app',
  templateUrl: 'gladpet.component.html',
  styleUrls: ['gladpet.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [ROUTER_PROVIDERS]
})

@Routes([
  new Route({path: '/', component: IndexComponent}),
  new Route({path: '/pet', component: PetComponent}),
  new Route({path: '/animal', component: AnimalComponent}),
])

export class GladpetAppComponent {

  title = 'gladpet works 11331!';

  constructor(private router: Router) {}

  ngOnInit() {
    //this.router.navigate(['/']);
  }
}
