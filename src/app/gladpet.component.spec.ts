import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { GladpetAppComponent } from '../app/gladpet.component';

beforeEachProviders(() => [GladpetAppComponent]);

describe('App: Gladpet', () => {
  it('should create the app',
      inject([GladpetAppComponent], (app: GladpetAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'gladpet works!\'',
      inject([GladpetAppComponent], (app: GladpetAppComponent) => {
    expect(app.title).toEqual('gladpet works!');
  }));
});
