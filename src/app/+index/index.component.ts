import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-index',
  templateUrl: 'index.component.html',
  styleUrls: ['index.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
export class IndexComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
