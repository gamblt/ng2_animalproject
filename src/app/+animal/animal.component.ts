import { Component, Input, SimpleChange, OnInit, OnChanges } from '@angular/core';
import { ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from '@angular/router';


export class Animal {
  name: string;
}


@Component({
  moduleId: module.id,
  selector: 'app-animal',
  templateUrl: 'animal.component.html',
  styleUrls: ['animal.component.css'],
  directives: [ROUTER_DIRECTIVES]
})

export class AnimalComponent implements OnChanges, OnInit {

  str: string;

  animal: Animal = {
    name: 'Windstorm'
  };

  constructor() {
    console.log('Init Animal component!');
  }

  ngOnInit() {
    console.log('OnInit Animals');
  }

  ngchange(event) {
    console.log('NgModelChange [%o]', event);
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange})
  {
    console.log('SimpleChange [%o]', changes);

    for (let propName in changes) {
      let chng = changes[propName];
      let cur  = JSON.stringify(chng.currentValue);
      let prev = JSON.stringify(chng.previousValue);
      console.log('Name[%o] Prev[%o] Cur[%o]', propName, prev, cur);
    }
  }

}
